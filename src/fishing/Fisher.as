package fishing 
{
	import fish.Fishies;
	import org.flixel.*;
	/**
	 * ...
	 * @author Lachlan
	 */
	
	public class Fisher extends FlxSprite
	{
		public var Health:int = 0;
		private static const MAX_HEALTH:int = 100;
		public var IsDead:Boolean = false;
		public var AnimationSpeed:Number;
		public var Elapser:Number = 0;
		public var FishCaught:Array;
		public var IsFishing:Boolean = false;
		public var FishCounter:int = 0;
		public var TotalFishWeight:Number = 0.0;
		
		public function Fisher() {
				Health = MAX_HEALTH/2;
				super(0, 0);
				loadGraphic(Assets.FISHERMAN_NOLINE);
		}
		public function create():void {
			FishCaught = new Array();
		}
		
		public function ModHealth(i:int = -1):void {
				Health += i;
				
			if (Health <= 0)
				IsDead = true;
		}
		
		public override function update():void {

			super.update();
		}
		
		
		//Fishing functions
		public function Cast():void {
			//Change from sitting to fishing animation.
			loadGraphic(Assets.FISHERMAN_LINE);
			IsFishing = true;
		}
		public function UnCast():void {
			loadGraphic(Assets.FISHERMAN_NOLINE);
			IsFishing = false;
		}
		public function ReadyToCatch():void {
			loadGraphic(Assets.FISHERMAN_TAUGHT_LINE);
		}
		
		public function CatchFish(weight:int):void {
			FishCounter++;
			TotalFishWeight += weight;
			ModHealth(weight);
			loadGraphic(Assets.FISHERMAN_WITH_FISH);
		}
		
		public function NumberOfFish():int {
			return FishCounter;
		}
		public function TotalWeight():Number {
			return TotalFishWeight;
		}
	}

}