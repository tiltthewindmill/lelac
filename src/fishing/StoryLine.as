package fishing 
{
	/**
	 * ...
	 * @author Lachlan
	 */
	public class StoryLine 
	{
		public var Line:String = "";
		public var PassageTGTYes:String = "";
		public var PassageTGTNo:String = "";
		public var Option1:String = "";
		public var Option2:String = "";
		public var HealthMod:int = 0;
		public var Ending:Boolean = false;
		public var Question:Boolean = false;
		public var Pause:Boolean = false;
		public var PauseTime:Number = 0;
		
		public function StoryLine(Line:String, Option1:String, PassageTGTYes:String, Option2:String, PassageTGTNo:String, HealthMod:int = 0, Ending:Boolean = false, Pause:Boolean = false, PauseTime:int = 0 ) 
		{
			this.Line = Line;
			this.PassageTGTYes = PassageTGTYes;
			this.PassageTGTNo = PassageTGTNo;
			this.Option1 = Option1;
			this.Option2 = Option2;
			this.HealthMod = HealthMod;
			this.Ending = Ending;
			if (this.PassageTGTNo == PassageTGTYes || this.PassageTGTNo == "-1" || this.Option2 == "-1")
				this.Question = false;
			else
				this.Question = true;
			
			this.Pause = Pause;
			this.PauseTime = PauseTime;
		}	
	}
}