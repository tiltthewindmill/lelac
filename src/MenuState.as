package
{
    import org.flixel.*;
 
    public class MenuState extends FlxState
    {
        public static var LEVEL_SIZE:FlxPoint = new FlxPoint(768, 512); // level size (in pixels)
        public static var BLOCK_SIZE:FlxPoint = new FlxPoint(32, 32); // block size (in pixels)
         
        public static var MENU:Menu = null;
         
        /**
         * Create state
         */
        override public function create():void 
        {
            FlxG.mouse.show();
            // load Menu
			
            MENU = new Menu(this,FlxG.width, FlxG.height);
            this.add(MENU);

        }
    }
}