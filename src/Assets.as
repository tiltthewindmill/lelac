package  
{
	/**
	 * ...
	 * @author Lachlan
	 */
	public class Assets 
	{
		//Story files
		[Embed(source = "../Story1.txt", mimeType = "application/octet-stream")] public static var STORY_1:Class;
		[Embed(source = "../Story2.txt", mimeType = "application/octet-stream")] public static var STORY_2:Class;
		[Embed(source = "../TestStory.txt", mimeType = "application/octet-stream")] public static var STORY_3:Class;
		
		//BG
		[Embed(source = "../LeLacBG.png")] public static var BG:Class; 
		
		//Fisherman
		//Three sprites: Line in, line out, fish on line
		[Embed(source = "../FisherManNoLine.png")] public static var FISHERMAN_NOLINE:Class;
		[Embed(source = "../FisherManLine.png")] public static var FISHERMAN_LINE:Class;
		[Embed(source = "../FisherManTaughtLine.png")] public static var FISHERMAN_TAUGHT_LINE:Class;
		[Embed(source = "../FisherManWithFish.png")] public static var FISHERMAN_WITH_FISH:Class;
		
		//Water effects
		[Embed(source = "../WaterRippleBig.png")] public static var WATER_RIPPLES:Class;
		
		//Fishies
		[Embed(source = "../Fishie.png")] public static var FISHIES_SHEET:Class; 
		[Embed(source = "../PlayerFishie.png")] public static var PLAYER_FISHIE_SHEET:Class; 
		[Embed(source = "../DeadFishie.png")] public static var DEAD_FISHIE:Class; 

		//Music
		[Embed(source = "../Le Lac.mp3")] public static var LELAC_MUSIC:Class;
		
		//Sounds
		[Embed(source = "../WaterLap.mp3")] public static var WATER_SOUNDS:Class;
		[Embed(source = "../ForestAmbience.mp3")] public static var FOREST_SOUNDS:Class;
	}

}