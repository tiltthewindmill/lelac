package  
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.utils.ByteArray;
	import flash.net.*;
	import flash.text.Font;
	import flash.utils.Dictionary;
	import mx.core.FlexSprite;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import fishing.*;
	/**
	 * ...
	 * @author Lachlan
	 */

	public class Fishing extends FlxGroup
	{
		//Play as fisherman
		/**
		 * You are a lone fisherperson
		 * You wish to sit at the side of a lake and spend your entire day there.
		 * You have thoughts. You can choose where these thoughts go by clicking responses.
		 * These control a "bar" of sorts. When it reaches zero you leave, leaving your fishing equipment behind.
		 * Bar starts white and goes to black.
		 * The fish are VERY slow biters but you can catch them.
		 * You can choose to leave whenever you wish.
		 * B&W
		 * 
		 * 
		 * How is this going to look?
		 * Back of fisher's head?
		 * Top down?
		 * I really need to draw this first..
		 * Okay, now we've sketched it, what shall we do next?
		 * 
		 * 
		 * Umm.
		 * Create some sort of background w/ perspective?
		 * Work on the player and their animations? (Also the occasional bird or something?)
		 * The story/interaction stuff?
		 * 
		 * I think the third one would be a nice place to start. Not in the mood to do art/animations.
		 * Well, the story interaction stuff is done. Can enhance the story engine to use a Dictionary.
		 * So, what's next?
		 * Placeholder art and animations. Along with backgrounds. Doing some of that. More story now.
		 * Have to add a new font, the default one garbles special chars.
		 * All fonts in Flash garble special chars, even doing several workarounds. Story will change to NOT have special chars :(
		 * So, what do we do now?
		 * Story pacing and fisherman interaction I think should be next. Maybe improving the story system before too much story has been written.
		 * Alternatively, move away from the story stuff for now until EVERY OTHER SYSTEM is done.
		 * Alright, that's the plan.
		 * Okay, you can now fish, just no animations exist yet.
		 * Revamping the story system
		 * Wow, haven't updated this in a while.
		 * So, story system revamped. Fishing is a little better (have animations.)
		 * I do want to add a pause story system. Also some form of reaction about catching a fish or whatever. 
		 * Oh my, haven't updated this for a bit.
		 * Almost done. Story system is nice, made some better design decisions wrt fishing.
		 * Fish eating minigame is now an easter egg.
		 * This story is tough to nut out.
		 * Music is good though...
		 * 
		 * Well, it's out now.
		 */
		
		protected var State:FlxState;
		public var LevelSize:FlxPoint;
		protected var BlockSize:FlxPoint;
		protected var LifeBar:FlxBar;
		protected var GUIGroup:FlxGroup;
		protected var FisherGroup:FlxGroup;
		protected var SceneGroup:FlxGroup;
		protected var TextGroup:FlxGroup;
		public var testInt:int = 100
		protected var FisherPerson:Fisher;
		protected var FishCounter:int = 0;
		protected var FishCounterText:FlxText;	//Deal with this soon
		protected var TotalElapser:Number = 0;
		protected var StoryElapser:Number = 0;
		protected var CatchElapser:Number = 0;
		
		private static const QUESTION_YES:String = "Yes";
		private static const QUESTION_NO:String = "No";
		private static const QUESTION_CONTINUE:String = "...";
		private static const QUESTION_END:String = "End";
		private static const FISH_CAUGHT:String = "Nice";

		//Story stuff
		protected var Story:Array;	//Each passage is a reference to an index
		protected var StoryDict:Object = null;
		protected var StoryContent:FlxText;
		public static var CurrentPassage:int = 0;
		public static var CurrentPassageName:String = "Start";
		protected var YesFlxText:FlxText;	//Can also be a continue
		protected var NoFlxText:FlxText;
		protected var HighlightRectangle:FlxSprite;
		protected var FONT:String = null;
		protected var EMBEDFONT:Boolean = true;
		protected var FontSize:int = 8;
		protected var StoryPaused:Boolean = false;
		protected var StoryPauseTime:Number = 0; //Who knows?
		
		//Fish stuff
		protected var TimeToCatchFish:Number;
		protected var FishReadyToCatch:Boolean = false;
		protected var FishCaught:Boolean = false;
		
		//Scenery
		protected var LeLacBackground:FlxSprite;
		protected var WaterRipples:FlxSprite;
		
		protected var IsDead:Boolean = false;
		
		protected var WaterSounds:FlxSound;
		protected var StoryStarted:Boolean = false;
		protected var FishieCorpse:FlxSprite;
		
		
		public function Fishing(State:FlxState, LevelSize:FlxPoint, BlockSize:FlxPoint) {
			super();
			
			this.State = State;
			this.LevelSize = LevelSize;
			this.BlockSize = BlockSize;
			FlxG.bgColor = 0xFFFFFFFF;
			
			//init groups
			GUIGroup = new FlxGroup();
			FisherGroup = new FlxGroup();
			SceneGroup = new FlxGroup();
			TextGroup = new FlxGroup();
			
			Story = new Array();
			StoryDict = new Object();
			
			
		}
		public function Start():void {
			FlxG.bgColor = 0xFFFFFFFF;
			create();
			//Music.play();
			
			FlxG.playMusic(Assets.LELAC_MUSIC, 0.65);
			FlxG.music.fadeIn(0.1);
			
		}
		
		public function create():void {
			NewFishTime();
			createScenery();
			createFisherPerson();
			createGUI();
			createText();
			
			add(SceneGroup);
			add(FisherGroup);
			add(GUIGroup);
			
			add(TextGroup);
		}
		
		public function createScenery():void {
			//Load trees
			LeLacBackground = new FlxSprite(0, 0, Assets.BG);
			SceneGroup.add(LeLacBackground);
			WaterRipples = new FlxSprite(0, 0);
			WaterRipples.loadGraphic(Assets.WATER_RIPPLES, true);
			WaterRipples.addAnimation("Rippling", [0, 1, 2, 3, 4], 5, true);	
		}

		public function createGUI():void {
			//Create LifeBar. This was in the orignal design. Didn't fit the final product.
			//LifeBar = new FlxBar(30, LevelSize.y - 15, FlxBar.FILL_LEFT_TO_RIGHT, LevelSize.y - 60, 15, FisherPerson, "Health");
			//LifeBar.createFilledBar(0xFF000000, 0xFFFFFFFF, true, 0xFF000000);
			//GUIGroup.add(LifeBar);
			
			HighlightRectangle = new FlxSprite(-100, -100);
			HighlightRectangle.makeGraphic(50, 2, 0xFF000000);
			GUIGroup.add(HighlightRectangle);
			
			FishieCorpse = new FlxSprite(40, 315, Assets.DEAD_FISHIE);
		}

		public function createFisherPerson():void {
			FisherPerson = new Fisher();
			FisherGroup.add(FisherPerson);
		}

		public function createText():void {
			//Init FlxTexts
			//Hover line would be useful.
			StoryContent = new FlxText(20, 10, LevelSize.x - 40);
			StoryContent.setFormat(FONT, FontSize, 0xFF000000, "center", 0); //A fade after clicking continue or y/n would be nice.
			
			YesFlxText = new FlxText(30, StoryContent.y + StoryContent.height + 5,150);
			YesFlxText.setFormat(FONT, FontSize, 0xFF000000, "left", 0);
			
			NoFlxText = new FlxText(LevelSize.x - 20 - 90, StoryContent.y + StoryContent.height + 5,150);
			NoFlxText.setFormat(FONT, FontSize, 0xFF000000, "left", 0);
			
			LoadStory(null);
			TextGroup.add(StoryContent);
			TextGroup.add(YesFlxText);
			TextGroup.add(NoFlxText);
		}

		public function LoadStory(storyName:Class):void {
			var byteArr:ByteArray;
			byteArr = new Assets.STORY_2();
			var storyString:String = byteArr.readMultiByte(byteArr.bytesAvailable, byteArr.endian);

			ParseStory(storyString);
			//Start story			
			IncrementStory("Start");
			PauseStory();
			StoryPauseTime = 5;	//Have a starting pause.
		}

		public function ParseStory(story:String):void {
			//The story parser. 

			var LineArray:Array = new Array();
			LineArray = story.split("\n");//Split lines
			var LineContentArray:Array = new Array();
			for (var i:int = 0; i < LineArray.length; i++)
			{
				LineContentArray = LineArray[i].split("|");
				//Find an ending marker.
				if (LineArray[i].search("::") > -1)
					StoryDict[LineContentArray[0]] = new StoryLine(LineContentArray[1], (LineContentArray[2].split("_"))[0], (LineContentArray[2].split("_"))[1], (LineContentArray[3].split("_"))[0], (LineContentArray[3].split("_"))[1], parseInt(LineContentArray[4]), true);
				else if (LineArray[i].search("//") > -1)
					StoryDict[LineContentArray[0]] = new StoryLine(LineContentArray[1], (LineContentArray[2].split("_"))[0], (LineContentArray[2].split("_"))[1], (LineContentArray[3].split("_"))[0], (LineContentArray[3].split("_"))[1], parseInt(LineContentArray[4]), false, true);
				else if (LineArray[i].search("HALT1") > -1)
					StoryDict[LineContentArray[0]] = new StoryLine(LineContentArray[1], (LineContentArray[2].split("_"))[0], (LineContentArray[2].split("_"))[1], (LineContentArray[3].split("_"))[0], (LineContentArray[3].split("_"))[1], parseInt(LineContentArray[4]), false, true, 10);
				else if (LineArray[i].search("HALT2") > -1)
					StoryDict[LineContentArray[0]] = new StoryLine(LineContentArray[1], (LineContentArray[2].split("_"))[0], (LineContentArray[2].split("_"))[1], (LineContentArray[3].split("_"))[0], (LineContentArray[3].split("_"))[1], parseInt(LineContentArray[4]), false, true, 15);
				else if (LineArray[i].search("HALT") > -1)
					StoryDict[LineContentArray[0]] = new StoryLine(LineContentArray[1], (LineContentArray[2].split("_"))[0], (LineContentArray[2].split("_"))[1], (LineContentArray[3].split("_"))[0], (LineContentArray[3].split("_"))[1], parseInt(LineContentArray[4]),false, true, 5);
				else
					StoryDict[LineContentArray[0]] = new StoryLine(LineContentArray[1], (LineContentArray[2].split("_"))[0], (LineContentArray[2].split("_"))[1], (LineContentArray[3].split("_"))[0], (LineContentArray[3].split("_"))[1], parseInt(LineContentArray[4]), false);
			}
		}
		
		public function IncrementStory(PassageToGoTo:String):void {
			if (StoryDict[CurrentPassageName].Pause){
				BlankText();
				StoryPauseTime = StoryDict[CurrentPassageName].PauseTime;
				StoryPaused = true;
				CurrentPassageName = PassageToGoTo;
			} else {
				CurrentPassageName = PassageToGoTo;
				DisplayCurrentStoryLine();
			}
		}
		
		public function DisplayCurrentStoryLine():void {
			StoryContent.text = StoryDict[CurrentPassageName].Line;
			
			if (StoryDict[CurrentPassageName].Question && !StoryDict[CurrentPassageName].Ending) {
				YesFlxText.x = 30;
				YesFlxText.text = StoryDict[CurrentPassageName].Option1;
				NoFlxText.text = StoryDict[CurrentPassageName].Option2; 
			} else if (!StoryDict[CurrentPassageName].Question && !StoryDict[CurrentPassageName].Ending) {
				YesFlxText.x = LevelSize.x / 2;
				YesFlxText.text = StoryDict[CurrentPassageName].Option1;
				NoFlxText.text = "";
			} else if (StoryDict[CurrentPassageName].Ending) {
				YesFlxText.x = LevelSize.x / 2;
				YesFlxText.text = QUESTION_END;
				NoFlxText.text = "";
			}

			FisherPerson.ModHealth(StoryDict[CurrentPassageName].HealthMod);

			HighlightRectangle.x = -100;
			HighlightRectangle.y = -100;
		}
		public function PauseStory():void {
			StoryPaused = true;
			BlankText();
		}
		public function BlankText():void {
			StoryContent.text = "";
			YesFlxText.text = "";
			NoFlxText.text = "";
			HighlightRectangle.x = -100;
			HighlightRectangle.y = -100;
		}

		public function LifeEnd():void {
			IsDead = true;
			FisherGroup.remove(FisherPerson);
			SceneGroup.remove(WaterRipples);
			TextGroup.kill();
			GUIGroup.kill();
			FlxG.music.fadeOut(5);
			FlxG.camera.fade(0xff000000, 5, HasDied);
		}
		
		public function CatchFish(weight:int = 1):void {
			FishCounter++;
			var FishieCorpseClone:FlxSprite = new FlxSprite(20 + (FishCounter * 6), FishieCorpse.y, Assets.DEAD_FISHIE);
			GUIGroup.add(FishieCorpseClone);
		}
		public function HasDied():void {
			//Something, like switch states. No message.
			FlxG.switchState(new MenuState());
		}
		
		public function NewFishTime():void {
			TimeToCatchFish = FlxG.random() * 50 + 24;
		}
		

		override public function update():void {
				TotalElapser = FlxG.elapsed;
				
			if (!FishReadyToCatch)
				CatchElapser += TotalElapser;
				
			if (StoryPaused && StoryPauseTime > 0 && !FishCaught)
				StoryElapser += TotalElapser;
				
			if (CatchElapser > TimeToCatchFish && FisherPerson.IsFishing){
				//TimeToCatchFish = FlxG.random() * 200 + 50;
				NewFishTime();
				FishReadyToCatch = true;
				CatchElapser = 0;
				FisherPerson.ReadyToCatch();
				//Reel in, re-cast
			}
			
			if (StoryElapser > StoryPauseTime && StoryPauseTime > 0) {
				StoryStarted = true;
				DisplayCurrentStoryLine();
				StoryElapser = 0;
				StoryPauseTime = 0;
				StoryPaused = false;
			}
			
			
			var mousePoint:FlxPoint = new FlxPoint();
			FlxG.mouse.getWorldPosition(null, mousePoint);
			if (YesFlxText.overlapsPoint(mousePoint) && !StoryPaused) {
				HighlightRectangle.x = YesFlxText.x;
				HighlightRectangle.y = YesFlxText.y + YesFlxText.height;
				HighlightRectangle.makeGraphic(YesFlxText.actualWidth, 2, 0xFF000000);
				if (FlxG.mouse.justPressed())
					if (YesFlxText.text != QUESTION_END && YesFlxText.text != FISH_CAUGHT)
						IncrementStory(StoryDict[CurrentPassageName].PassageTGTYes);
					else if (YesFlxText.text == FISH_CAUGHT)
						DisplayCurrentStoryLine();
					else
						LifeEnd();
			} else if (NoFlxText.overlapsPoint(mousePoint) && StoryDict[CurrentPassageName].Question  && !StoryPaused) {
				HighlightRectangle.x = NoFlxText.x;
				HighlightRectangle.y = NoFlxText.y + NoFlxText.height;
				HighlightRectangle.makeGraphic(NoFlxText.actualWidth, 2, 0xFF000000);
				if (FlxG.mouse.justPressed())
					IncrementStory(StoryDict[CurrentPassageName].PassageTGTNo);
			} else if (FlxG.mouse.justPressed() && !NoFlxText.overlapsPoint(mousePoint) && !YesFlxText.overlapsPoint(mousePoint) && StoryStarted) {
				if (!FisherPerson.IsFishing){
					FisherPerson.Cast();
					SceneGroup.add(WaterRipples);
				} else if (FisherPerson.IsFishing && FishReadyToCatch) {
					FishReadyToCatch = false;
					FishCaught = true;
					var CatchString:String = PlayState.FISH.CatchFish();
					var CatchStringSplit:Array = CatchString.split(":");
					SceneGroup.remove(WaterRipples);
					if (CatchStringSplit[0] == "PlayerFish") {
						//Hooray. Do story stuff or something. Reaction/emotion...
						FisherPerson.CatchFish(parseInt(CatchStringSplit[1]));
					} else {
						FisherPerson.CatchFish(parseInt(CatchStringSplit[1]));
					}
					
				} else if (FisherPerson.IsFishing && FishCaught) {
					CatchFish();
					FisherPerson.UnCast();
					FishCaught = false;
				} 
				
				if (StoryPaused && StoryPauseTime <= 0) {
					DisplayCurrentStoryLine();
					StoryPaused = false;
				}
			}
			
			if (FisherPerson.IsFishing)
				WaterRipples.play("Rippling");
				
			if (FisherPerson.Health <= 0)
				LifeEnd();
				
			super.update();
		}
		
	}

}