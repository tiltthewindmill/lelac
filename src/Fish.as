package  
{
	import org.flixel.*;
	import fish.*;
	/**
	 * ...
	 * @author Lachlan
	 */
	public class Fish extends FlxGroup
	{
		//Play as fish
		/**
		 * You start as a random fish
		 * You can move around and eat little things
		 * You can also eat other fish. This will make you hated by the other fish.
		 * It is possible for all the other fish to eat you if they don't like you much.
		 * Other fish can get the idea to eat other fish as well.
		 * Health is regained by eating food, not fish.
		 * Can new fish appear? No that would be silly for now.
		 * B&W. (Maybe. Would look much "cleaner")
		 * Food!
		 * 
		 * How do the two games connect? Quantity of fish determines likelihood of catching something?
		 * If fishing has started but go into fish, you can eat the lure?
		 */
		
		protected var FishGroup:FlxGroup;
		protected var GUIGroup:FlxGroup;
		protected var FoodGroup:FlxGroup;
		
		protected var State:FlxState;
		public var LevelSize:FlxPoint;
		protected var BlockSize:FlxPoint;
		protected var FishOfControl:PlayerFish;

		protected var FishAddDelta:Number = 0;
		protected var FoodAddDelta:Number = 0;
		protected var FoodElapser:Number = 0;
		protected var FishElapser:Number = 0;
		protected var FishEatElapser:Number = 0;
		protected var ElapserKeeper:Number = 0;
		public var EatingFlag:Boolean = false;
		public var FishEatingSpeed:Number = 0.88;
		
		public function Fish(State:FlxState, LevelSize:FlxPoint, BlockSize:FlxPoint) {
			super();
			this.State = State;
			this.LevelSize = LevelSize;
			this.BlockSize = BlockSize;
			FlxG.bgColor = 0xFFFFFFFF;
			FishGroup = new FlxGroup();
			FoodGroup = new FlxGroup();

			FishAddDelta = FlxG.random() * 7 + 1;
			FoodAddDelta = FlxG.random() * 4.5 + 0.5;
			
			InitialiseWorld();
			for (var i:int = 0; i < 23; i++) {
				AddNewFishie();
			}
			for (i = 0; i < 5; i++) {
				AddMoreFood();
			}
			CreatePlayerFish();
		}
		
		public function InitialiseWorld():void {
			//Create a default amount of fish and food
			add(FishGroup);
			add(FoodGroup);
			FishBrains.InitBrains();
		}
		
		public function CreatePlayerFish():void {
			FishOfControl = new PlayerFish(this);
			add(FishOfControl);
		}
		
		public function AddNewFishie():void {
			var newFishie:Fishies = new Fishies(this);
			FishGroup.add(newFishie);
		}
		public function AddMoreFood():void {
			var FishFood:FlxSprite = new FlxSprite(FlxG.random() * LevelSize.x, FlxG.random() * LevelSize.y);
			FishFood.makeGraphic(4, 4, 0xFA003002);
			FoodGroup.add(FishFood);
		}
		
		protected function createCamera():void {
			FlxG.worldBounds = new FlxRect(0, 0, FlxG.width, FlxG.height);
			FlxG.camera.setBounds(0, 0, LevelSize.x, LevelSize.y, false);
            FlxG.camera.follow(FishOfControl, FlxCamera.STYLE_TOPDOWN_TIGHT);
		}

		override public function update():void{
			ElapserKeeper = FlxG.elapsed;
			//Very strange bug here.
			FoodElapser += ElapserKeeper;
			FishElapser += ElapserKeeper;
			FishEatElapser += ElapserKeeper;
			if (FoodElapser > FoodAddDelta){
				AddMoreFood();
				FoodElapser = 0;
				FoodAddDelta = FlxG.random() * 4.5 + 0.5;
			}

			if (FishElapser > FishAddDelta){
				AddNewFishie();
				FishElapser = 0;
				FishAddDelta = FlxG.random() * 12 + 2;
			}

			FlxG.collide(FishOfControl, FishGroup,EatOtherFish);
			EatFoodCheck();
			FlxG.collide(FishGroup, FoodGroup, FishiesEatFoodCheck);

			super.update();			
		}
		
		public function EatOtherFish(o1:Object, o2:Object):void{
			if (EatingFlag){
				for (var i:int = 0; i < FishGroup.length; i++){
					if (FlxG.overlap(FishOfControl, FishGroup.members[i])){
						if (FishEatElapser > FishEatingSpeed){
							FishGroup.members[i].DecrHealth();
							FishEatElapser = 0;
						}
						if (FishGroup.members[i].IsDead)
							FishGroup.remove(FishGroup.members[i],true);
						break;
					}
				}
			}
		}

		public function EatFoodCheck():void{
			//incr player health
			for (var i:int = 0; i < FoodGroup.length; i++)
			{
				if (FlxG.overlap(FishOfControl, FoodGroup.members[i]))
				{
					EatFood(FoodGroup.members[i]);
					break;
				}
			}
		}

		public function EatFood(FishFood:FlxSprite):void{
			FishOfControl.IncrHealth();
			FoodGroup.remove(FishFood);
		}

		public function FishiesEatFoodCheck(o1:Object, o2:Object):void{
			var breaking:Boolean = false;
			//stop fish movement, incr health
			for (var i:int = 0; i < FishGroup.length; i++)
			{
				for (var j:int = 0; j < FoodGroup.length; j++)
				{
					if (FlxG.overlap(FishGroup.members[i], FoodGroup.members[j]))
					{
						FishGroup.members[i].Eat();
						FoodGroup.remove(FoodGroup.members[j]);
						breaking = true;
						break;
					}
				}
				if (breaking)
					break;
			}
		}
		
		public function CatchFish():String
		{
			//Determine if fish is from FishGroup or FishOfControl
			if (FlxG.random() < 0.07)
				return "PlayerFish:" + FishOfControl.weight;
			else{
				var randomer:int = FlxG.random() * FishGroup.length;
				var FishieReturn:Fishies = FishGroup.members[randomer];
				FishGroup.remove(FishieReturn, true);
				return "Fishie:" + FishieReturn.weight;
			}
		}
	}
}