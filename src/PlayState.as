package  
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Lachlan
	 */
	public class PlayState extends FlxState
	{
		
		public static var LEVEL_SIZE:FlxPoint = new FlxPoint(930, 750); // level size (in pixels)
        public static var BLOCK_SIZE:FlxPoint = new FlxPoint(10, 10); // block size (in pixels)
		public static var FISH:Fish = null;
		public static var FISHING:Fishing = null;
		public function PlayState() {}
		
		override public function create():void
		{
			FlxG.mouse.show();
			var LevelSize:FlxPoint = new FlxPoint(FlxG.width, FlxG.height);
			
			FISHING = new Fishing(this, LevelSize, BLOCK_SIZE);
			FISH = new Fish(this, LevelSize, BLOCK_SIZE);
            FISHING.Start();
			this.add(FISHING);
		}	
	}
}