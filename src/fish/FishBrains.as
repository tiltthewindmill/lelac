package fish 
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Lachlan
	 */
	public class FishBrains 
	{
		public static var HeadingOptions:Array = new Array();
		public static var deltaTime:Number = 6;
		public static const UP_HEADING:String = "UP";
		public static const DOWN_HEADING:String = "DOWN";
		public static const LEFT_HEADING:String = "LEFT";
		public static const RIGHT_HEADING:String = "RIGHT";
		public static const HOLD_HEADING:String = "HOLD";
		
		public static function InitBrains():void
		{
			HeadingOptions.push(UP_HEADING);
			HeadingOptions.push(DOWN_HEADING);
			HeadingOptions.push(LEFT_HEADING);
			HeadingOptions.push(RIGHT_HEADING);
			HeadingOptions.push(HOLD_HEADING);
		}
		
		public static function getHeading(headPos:int = 0):String
		{
			return HeadingOptions[headPos];
		}
		
		public static function Delta(FishType:String = "Fish"):Number
		{
			//Different deltas for different fish
			return deltaTime;
		}
		
	}

}