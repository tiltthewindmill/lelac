package fish 
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Lachlan
	 */
	public class PlayerFish extends FlxSprite
	{
		public static const SIZE:FlxPoint = new FlxPoint(10, 10); // size in pixels
		public static const CENTRE_POINT:FlxPoint = new FlxPoint(SIZE.x/2, SIZE.y/2);
    	public static const RUNSPEED:int = 28;
		public static const DRIFTSPEED:int = 5;
		public static const MAX_VELOCITY:FlxPoint = new FlxPoint(RUNSPEED, RUNSPEED);
		public static const ACCEL_RATE:int = 4;
		public static const DRAG_RATE:int = 1;
		public static const MAX_HEALTH:int = 10;
		protected var CurrHealth:int;
		protected var FISH:Fish = null;
		public var weight:Number = 1.5;


		public function PlayerFish(FISH:Fish) 
		{
			super(3, 3);
			this.FISH = FISH;
			//makeGraphic(SIZE.x, SIZE.y, 0xFFFF3366);
			loadGraphic(Assets.PLAYER_FISHIE_SHEET, true, true,10,5);
			drag = new FlxPoint(DRIFTSPEED * DRAG_RATE, DRIFTSPEED * DRAG_RATE);
			maxVelocity = MAX_VELOCITY;
			CurrHealth = MAX_HEALTH;
		}
		
		public function IncrHealth():void
		{
			if (CurrHealth < MAX_HEALTH)
				CurrHealth++;
			weight += 0.1;
			//Update speed. Become too slow (but never zero.)
			//When weight exceeds x, switch back to fishing.
		}
		
		
		public override function update():void {
			updateControls();
			//Drag barrier checking
			if (x > this.FISH.LevelSize.x - SIZE.x)
				x = this.FISH.LevelSize.x - SIZE.x;
			else if (x < 0)
				x = 0;
			if (y > this.FISH.LevelSize.y - SIZE.y)
				y = this.FISH.LevelSize.y - SIZE.y
			else if (y < 0)
				y = 0;
			super.update();
        }
		
		//Will add mouse movement (click to move.)
		protected function updateControls():void {
			acceleration.x = acceleration.y = 0; // no gravity or drag by default
			var movement:FlxPoint = new FlxPoint();

			if ((FlxG.keys.pressed("LEFT") || FlxG.keys.pressed("A")) && x > 0)
				movement.x -= 1;
			if ((FlxG.keys.pressed("RIGHT") || FlxG.keys.pressed("D")) && x < this.FISH.LevelSize.x - SIZE.x)
				movement.x += 1;
			if ((FlxG.keys.pressed("UP") || FlxG.keys.pressed("W")) && y > 0)
				movement.y -= 1;
			if ((FlxG.keys.pressed("DOWN") || FlxG.keys.pressed("S")) && y < this.FISH.LevelSize.y - SIZE.y)
				movement.y += 1;
			// check final movement direction
			if (movement.x < 0)
				moveLeft();
			else if (movement.x > 0)
				moveRight();
			if (movement.y < 0)
				moveUp();
			else if (movement.y > 0)
				moveDown();
			if (FlxG.keys.justPressed("SPACE"))
				this.FISH.EatingFlag = true;
			if (FlxG.keys.justReleased("SPACE"))
				this.FISH.EatingFlag = false;
				
			//Space to eat food.
			//Hold E to eat fish.
			
			play("swimHoriz_0");

		}

		
		/**
         * Move entity left
         */
        public function moveLeft():void {
            facing = LEFT;
            acceleration.x = -RUNSPEED * ACCEL_RATE; // accelerate to top speed in 1/4 of a second
        }
         
        /**
         * Move entity right
         */
        public function moveRight():void {
            facing = RIGHT;
            acceleration.x = RUNSPEED * ACCEL_RATE; // accelerate to top speed in 1/4 of a second
        }
         
        /**
         * Move entity up
         */
        public function moveUp():void {
            facing = UP;
            acceleration.y = -RUNSPEED * ACCEL_RATE; // accelerate to top speed in 1/4 of a second
        }
         
        /**
         * Move playe rdown
         */
        public function moveDown():void {
            facing = DOWN;
            acceleration.y = RUNSPEED * ACCEL_RATE; // accelerate to top speed in 1/4 of a second
        }
		
		//Animation stuff
		public function createAnimations():void
		{
			addAnimation("swimHoriz_0",[0,1,2],4);
		}
	
    }
	}