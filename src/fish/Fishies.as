package fish 
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Lachlan
	 */
	public class Fishies extends FlxSprite
	{
		protected var FishType:String;
		protected var FishImage:Class;
		
		public static const SIZE:FlxPoint = new FlxPoint(10, 10); // size in pixels
		public static const CENTRE_POINT:FlxPoint = new FlxPoint(SIZE.x/2, SIZE.y/2);
    	public static const RUNSPEED:int = 15;
		public static const DRIFTSPEED:int = 1;
		public static const MAX_VELOCITY:FlxPoint = new FlxPoint(RUNSPEED, RUNSPEED);
		public static const ACCEL_RATE:int = 4;
		public static const DRAG_RATE:int = 1;
		public var weight:Number = 1.5;
		
		protected static const MAX_HEALTH:int = 10;
		protected var FISH:Fish = null;
		public var Heading:String;
		protected var HeadingOptions:Array;
		protected var currTime:Number = 0;
		protected var CurrHealth:int;

		protected var eating:Boolean = false;
		public var IsDead:Boolean = false;
		
		
		//These fish can get angry and even eat each other!
		public function Fishies(FISH:Fish = null) 
		{
			this.FISH = FISH;
			super(FlxG.random()*FISH.LevelSize.x, FlxG.random()*FISH.LevelSize.y);
			//makeGraphic(10, 10, 0xFF74D5BD);
			loadGraphic(Assets.FISHIES_SHEET, true, true, 10, 5);
			drag = new FlxPoint(DRIFTSPEED * DRAG_RATE, DRIFTSPEED * DRAG_RATE);
			maxVelocity = MAX_VELOCITY;
			Heading = FishBrains.getHeading(FlxG.random() * 4); //Will remove hard-cody stuff later
			CurrHealth = MAX_HEALTH;
			createAnimations();
		}
		
		public function initFish(FishType:String, FishImage:Class):void
		{
			this.FishType = FishType;
			this.FishImage = FishImage;
			loadGraphic(FishImage);
			
		}
		public function IncrHealth():void
		{
			if (CurrHealth < MAX_HEALTH)
				CurrHealth++;
		}
		public function DecrHealth():void {
			if (CurrHealth < 0)
				IsDead = true;
			else
				CurrHealth--;
		}
		public function Eat():void
		{
			Heading = FishBrains.HOLD_HEADING;
			eating = true;
			IncrHealth();
			var Timer:FlxTimer = new FlxTimer();
			Timer.start(3,0,FinishFood);
		}
		public function FinishFood(Timer:FlxTimer):void
		{
			eating = false;
		}
		
		public override function update():void {
			updateControls();
			//Drag barrier checking. Not greatly efficient or even correct for these fish.
			//This keeps calling, but it works (to a degree.) Need to improve
			if (x > this.FISH.LevelSize.x - SIZE.x){
				//x = this.FISH.LevelSize.x - SIZE.x;
				Heading = FishBrains.getHeading(FlxG.random() * 4);
				currTime = 0;
			}else if (x < 0){
				//x = 0;
				Heading = FishBrains.getHeading(FlxG.random() * 4);
				currTime = 0;
			}
			if (y > this.FISH.LevelSize.y - SIZE.y){
				//y = this.FISH.LevelSize.y - SIZE.y
				Heading = FishBrains.getHeading(FlxG.random() * 4);
				currTime = 0;
			}else if (y < 0){
				//y = 0;
				Heading = FishBrains.getHeading(FlxG.random() * 4);
				currTime = 0;
			}

			if (!eating)
				currTime += FlxG.elapsed;

			if (currTime > FishBrains.Delta(FishType))
			{
				Heading = FishBrains.getHeading(FlxG.random() * 4); //Will remove hard-cody stuff later
				currTime = 0;
			}
			super.update();
        }
		
		public function updateControls():void
		{
			acceleration.x = acceleration.y = 0; // no gravity or drag by default
			var movement:FlxPoint = new FlxPoint();
			
			if (Heading == "LEFT" && x > 0)
				movement.x -= 1;
			if (Heading == "RIGHT" && x < this.FISH.LevelSize.x - SIZE.x)
				movement.x += 1;
			if (Heading == "UP"  && y > 0)
				movement.y -= 1;
			if (Heading == "DOWN" && y < this.FISH.LevelSize.y - SIZE.y)
				movement.y += 1;
			// check final movement direction
			if (movement.x < 0)
				moveLeft();
			else if (movement.x > 0)
				moveRight();
			if (movement.y < 0)
				moveUp();
			else if (movement.y > 0)
				moveDown();
				
			play("swimHoriz_0");
		}
		
		/**
         * Move entity left
         */
        public function moveLeft():void {
            facing = LEFT;
            acceleration.x = -RUNSPEED * ACCEL_RATE; // accelerate to top speed in 1/4 of a second
        }
         
        /**
         * Move entity right
         */
        public function moveRight():void {
            facing = RIGHT;
            acceleration.x = RUNSPEED * ACCEL_RATE; // accelerate to top speed in 1/4 of a second
        }
         
        /**
         * Move entity up
         */
        public function moveUp():void {
            facing = UP;
            acceleration.y = -RUNSPEED * ACCEL_RATE; // accelerate to top speed in 1/4 of a second
        }
         
        /**
         * Move playe rdown
         */
        public function moveDown():void {
            facing = DOWN;
            acceleration.y = RUNSPEED * ACCEL_RATE; // accelerate to top speed in 1/4 of a second
        }
		
		//Animation stuff
		public function createAnimations():void
		{
			addAnimation("swimHoriz_0",[0,1,2],4);

		}
		
	}

}