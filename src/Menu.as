package 
{

	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import flash.system.System;

	public class Menu extends FlxGroup
	{
		private var background:FlxSprite;
		private var newButton:FlxButtonPlus;
		private var loadButton:FlxButtonPlus;
		private var helpButton:FlxButtonPlus;
		private var quitButton:FlxButtonPlus;
		private var State:FlxState;
		public var WaterSounds:FlxSound;
		public var ForestSounds:FlxSound;

		public function Menu(State:FlxState, Width:int, Height:int):void
		{
			this.State = State;
			background = new FlxSprite(0,0);
			background.loadGraphic(Assets.BG);
			add(background);
			
			var heading:FlxText = new FlxText(Width/2 - 50, 10, 100, "Le Lac");
			heading.setFormat(null, 16, 0xFF000000,"center",0xFF858585);
			add(heading)

			newButton = new FlxButtonPlus(Width/2 - 50, Height / 2 - 40, Start, null, "Start", 100,30);
			newButton.updateInactiveButtonColors([0xFF858585,0xFFFFFFFF]);
			newButton.updateActiveButtonColors([0xFFD1D1D1,0xFFFFFFFF]);
			add(newButton);
			
			quitButton = new FlxButtonPlus(Width / 2 - 50, Height / 2 + 60, Quit, null, "Quit", 100, 30);
			quitButton.updateInactiveButtonColors([0xFF858585,0xFFFFFFFF]);
			quitButton.updateActiveButtonColors([0xFFD1D1D1,0xFFFFFFFF]);
			add(quitButton)
			
			WaterSounds = new FlxSound();
			WaterSounds.loadEmbedded(Assets.WATER_SOUNDS, true);
			WaterSounds.volume = 0.4;
			WaterSounds.play();
			
			ForestSounds = new FlxSound();
			ForestSounds.loadEmbedded(Assets.FOREST_SOUNDS, true);
			ForestSounds.volume = 0.4;
			ForestSounds.play();
		}

		public function Start():void
		{
			//Clean
			FlxG.switchState(new PlayState());
		}
		public function Quit():void
		{
			System.exit(0);
		}
	}
}